var appRouter = function(app) {
    var controllerObj = require("../controllers/controller.js");
    app.route("/api/rest/addUserQuestion").post(controllerObj.addUserQuestion);
    app.route("/api/rest/addUserAnswer").post(controllerObj.addUserAnswer);
    app.route("/api/rest/getAnswer").post(controllerObj.getAnswer);
}
module.exports = appRouter;