var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;

var url = "mongodb://localhost:27017/";
var dbo;

//create mongoDB and collection 
MongoClient.connect(url, function(err, db) {
    dbo = db.db("forum");
    console.log("mongoDB Connected..!");
    dbo.createCollection("questions");
    dbo.createCollection("answers");
});
exports.addQuestion = function(reqObj, res, cbk) {
    dbo.collection('questions').update({
        $and: [
            { questionId: reqObj.questionId },
            { userId: reqObj.userId }
        ]
    }, reqObj, { upsert: true }, function(err, result) {
        cbk(null, result);
    });
}
exports.addAnswer = function(reqObj, res, cbk) {
    var ansDescriptionObj = {};
    ansDescriptionObj.answerId = reqObj.answerId;
    ansDescriptionObj.userId = reqObj.userId;
    ansDescriptionObj.description = reqObj.description;
    ansDescriptionObj.postedDate = new Date();
    dbo.collection('answers').find({ questionId: reqObj.questionId }, { projection: { _id: 0 } }).toArray(function(err, results) {
        if (results.length) {
            dbo.collection('answers').find({
                $and: [{ questionId: reqObj.questionId }, {
                    answerDescription: {
                        $elemMatch: {
                            $and: [{ answerId: ansDescriptionObj.answerId }, { userId: ansDescriptionObj.userId }]
                        }
                    }
                }]
            }).toArray(function(err, results) {
                console.log(results);
                if (results.length) {
                    console.log("update array of object");
                    dbo.collection('answers').updateOne({
                        $and: [{ questionId: reqObj.questionId }, {
                            answerDescription: {
                                $elemMatch: {
                                    $and: [{ answerId: ansDescriptionObj.answerId }, { userId: ansDescriptionObj.userId }]
                                }
                            }
                        }]
                    }, { $set: { 'answerDescription.$': ansDescriptionObj } }, function(err, result) {
                        cbk(null, result);
                    })
                } else {
                    console.log("push")
                    dbo.collection('answers').update({ questionId: reqObj.questionId }, { $push: { "answerDescription": ansDescriptionObj } }, function(err, result) {
                        cbk(null, result);
                    });
                }
            });
        } else {
            var newQuesObj = {};
            newQuesObj.questionId = reqObj.questionId;
            newQuesObj.answerDescription = [];
            newQuesObj.answerDescription.push(ansDescriptionObj);
            console.log("new");
            dbo.collection('answers').insert(newQuesObj, function(err, result) {
                cbk(null, result);
            });
        }
    });
}
exports.getAnswerByQuesId = function(req, res, cbk) {
    var query = { "questionId": req.body.QuestionId };
    dbo.collection('answers').find(query, { projection: { _id: 0 } }).toArray(function(err, result) {
        cbk(null, result);
    });
}