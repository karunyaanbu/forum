var modelObj = require("../models/model.js");
exports.addUserQuestion = function(req, res) {
    var contype = req ? req.headers['content-type'] : "";
    console.log("Question Content type : " + contype);
    if (contype && contype.indexOf('application/json') >= 0) {
        var requestBody = req.checkBody;
        if (requestBody('QuestionId', 'QuestionId is required').notEmpty()) {
            requestBody('QuestionId', 'QuestionId is not valid').isInt();
        }
        requestBody('UserId', 'UserId is required').notEmpty();
        if (requestBody('RelatedTags', 'Tag is required').notEmpty()) {
            requestBody('RelatedTags', 'Tag should be array').isArray();
        }
        requestBody('Title', 'Title is required').notEmpty();
        requestBody('Description', 'Question Description is required').notEmpty();
        var errors = req.validationErrors();
        if (errors) {
            res.send(errors);
        } else {
            var reqObj = {};
            reqObj.questionId = req.body.QuestionId;
            reqObj.userId = req.body.UserId;
            reqObj.relatedTags = req.body.RelatedTags;
            reqObj.title = req.body.Title;
            reqObj.description = req.body.Description;
            reqObj.postedDate = new Date();
            modelObj.addQuestion(reqObj, res, function(err, result) {
                if (err) {
                    res.status(400).json({ message: 'Failure' });
                } else {
                    res.status(200).json({ message: "Question updated sucessfully", status: 'Success' });
                }
            });
        }
    } else {
        res.status(400).json({ message: 'Bad Request', status: 'Failure' });
    }
}
exports.addUserAnswer = function(req, res) {
    var contype = req ? req.headers['content-type'] : "";
    console.log("Answer Content type : " + contype);
    if (contype && contype.indexOf('application/json') >= 0) {
        var requestBody = req.checkBody;
        if (requestBody('QuestionId', 'QuestionId is required').notEmpty()) {
            requestBody('QuestionId', 'QuestionId is not valid').isInt();
        }
        if (requestBody('AnswerId', 'AnswerId is required').notEmpty()) {
            requestBody('AnswerId', 'AnswerId is not valid').isInt();
        }
        if (requestBody('UserId', 'UserId is required').notEmpty()) {
            requestBody('UserId', 'UserId is not valid').isInt();
        }
        requestBody('Description', 'Description is required').notEmpty();
        var errors = req.validationErrors();
        if (errors) {
            res.send(errors);
        } else {
            var reqObj = {};
            reqObj.questionId = req.body.QuestionId;
            reqObj.answerId = req.body.AnswerId;
            reqObj.userId = req.body.UserId;
            reqObj.description = req.body.Description;
            modelObj.addAnswer(reqObj, res, function(err, result) {
                if (err) {
                    res.status(400).json({ message: 'Failure' });
                } else {
                    res.status(200).json({ message: "Answer updated sucessfully", status: 'Success' });
                }
            });
        }
    } else {
        res.status(400).json({ message: 'Bad Request', status: 'Failure' });
    }
}
exports.getAnswer = function(req, res) {
    var contype = req ? req.headers['content-type'] : "";
    if (contype && contype.indexOf('application/json') >= 0) {
        var requestBody = req.checkBody;
        if (requestBody('QuestionId', 'QuestionId is required').notEmpty()) {
            requestBody('QuestionId', 'QuestionId should be integer').isInt();
        }
        var errors = req.validationErrors();
        if (errors) {
            res.send(errors);
        } else {
            modelObj.getAnswerByQuesId(req, res, function(err, result) {
                if (err) {
                    res.status(400).json({ message: 'Failure' });
                } else {
                    res.status(200).send(result);
                }
            });
        }
    } else {
        res.status(400).json({ message: 'Bad Request', status: 'Failure' });
    }
}