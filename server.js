var http = require('http');
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
// calling our installed modules and assigning them to variable
var express = require('express');
var bodyParser = require('body-parser');
var routes = require("./routes/routes.js");
const expressValidator = require('express-validator');
const session = require('express-session');
//to accept both JSON and url encoded values
var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressValidator());
routes(app);

var server = app.listen(3000, function() {
    console.log("app running on port.", server.address().port);
});